import { promises as fs } from 'fs';

import { Parcel } from '@parcel/core';

(async () => {
    const { port } = new URL(JSON.parse(await fs.readFile('./src-tauri/tauri.conf.json', 'utf8'))['build']['devPath']);
    await new Parcel({
        entries: 'src-vue/index.html',
        defaultConfig: '@parcel/config-default',
        env: { NODE_ENV: 'development' },
        serveOptions: { port },
        hmrOptions: { port }
    }).watch();
})().catch(console.error);