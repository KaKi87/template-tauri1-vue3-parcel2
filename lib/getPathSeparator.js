import {
    path
} from '@tauri-apps/api';

let _pathSeparator;

export default async () => {
    if(!_pathSeparator)
        _pathSeparator = await path.join('');
    return _pathSeparator;
};