import { os, shell } from '@tauri-apps/api';

let _command, _paramPrefix;

export default async command => {
    if(!_command && !_paramPrefix){
        const platform = await os.platform();
        _command = {
            'linux': 'sh',
            'win32': 'cmd'
        }[platform];
        _paramPrefix = {
            'linux': '-',
            'win32': '/'
        }[platform];
    }
    return await new shell.Command(_command, [`${_paramPrefix}c`, command]).execute();
};